import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import fetchJsonp from 'fetch-jsonp';
import Moment from 'moment'

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'https://www.googleapis.com';
const API_SUGGESTIONS = 'https://suggestqueries.google.com/complete/search';
const API_KEY = 'AIzaSyDejB4BUWmpXoGc4I8tiwz5lpeB48A1KFs';

const videoQueries = {
    search: debounce((input, callback, filters = {}) => {
        const url = '/youtube/v3/search';
        const urlWithStatistics = '/youtube/v3/videos';

        let query = {
            'maxResults': '25',
            'part': 'snippet',
            'type': 'video',
            'q': input,
            'key': API_KEY
        };

        if (filters.category) query.videoCategoryId = filters.category;
        if (filters.year) query.publishedAfter = Moment(`${filters.year}-01-01`).format();
        if (filters.year) query.publishedBefore = Moment(`${filters.year}-12-31`).format();

        superagent.get(`${API_ROOT}${url}`).query(query).then(response => {
            const videoIds = response.body.items.map(item => {
                return item.id.videoId
            });
            superagent.get(`${API_ROOT}${urlWithStatistics}`).query({
                'id': videoIds.toString(),
                'part': 'snippet,contentDetails,statistics',
                'key': API_KEY
            }).then(response => {
                callback(response.body.items);
            });
        })
    }, 1000),
    getPopularVideos: debounce((callback, filters = {}) => {
        const url = '/youtube/v3/videos';
        let query = {
            'chart': 'mostPopular',
            'regionCode': 'RO',
            'part': 'snippet,contentDetails,statistics',
            'maxResults': 20,
            'key': API_KEY
        };

        if (filters.category) query.videoCategoryId = filters.category;
        if (filters.year) query.publishedAfter = Moment(`${filters.year}-01-01`).format();
        if (filters.year) query.publishedBefore = Moment(`${filters.year}-12-31`).format();

        superagent.get(`${API_ROOT}${url}`).query(query).then(response => {
            callback(response.body.items);
        })
    }, 1000),
    getSuggestions: (input, callback) => {
        fetchJsonp(`${API_SUGGESTIONS}?client=youtube&ds=yt&q=${input}`)
            .then(function (response) {
                return response.json()
            }).then(function (json) {
                const response = json[1].map(item => {
                    return { name: item[0], id: item[0].toLowerCase().trim() }
                });
                callback(response);
            }).catch(function (ex) {
                console.error('parsing failed', ex)
            })
    },
    getCategories: callback => {
        const url = '/youtube/v3/videoCategories';
        superagent.get(`${API_ROOT}${url}`).query({
            'regionCode': 'RO',
            'part': 'snippet',
            'key': API_KEY
        }).then(response => {
            callback(response.body.items);
        })
    }
}

function debounce(a, b, c) {
    var d, e;
    return function () {
        function h() { d = null, c || (e = a.apply(f, g)) }
        var f = this, g = arguments;
        return clearTimeout(d), d = setTimeout(h, b), c && !d && (e = a.apply(f, g)), e
    }
}

export default videoQueries;