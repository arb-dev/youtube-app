import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Header from '../Header';
import Home from '../Home/Home';
import Grid from '../Grid/Grid';
import './App.css';
import agent from '../../agent';

const mapStateToProps = state => {
  return {
    videos: state.videos
  }
};

const mapDispatchToProps = dispatch => ({
  getPopularVideos: payload => dispatch({ type: 'GET_VIDEOS', payload }),
  getCategories: payload => dispatch({ type: 'GET_CATEGORIES', payload }),
});

class App extends Component {

  componentWillMount() {
    agent.getPopularVideos(this.props.getPopularVideos);
    agent.getCategories(this.props.getCategories);
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/videos" component={Grid} />
          <Route path="/video/:id" component={Grid} />
        </Switch>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
