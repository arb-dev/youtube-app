import React from 'react';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
import { connect } from 'react-redux';
import agent from '../../agent';

const mapStateToProps = state => {
  return {
    categories: state.categories,
    selectedCategory: state.selectedCategory,
    year: state.year,
    searchInput: state.searchInput,
  }
};

const mapDispatchToProps = dispatch => ({
  selectCategory: category => dispatch({ type: 'GET_CATEGORY', category }),
  selectYear: year => dispatch({ type: 'GET_YEAR', year }),
  getPopularVideos: payload => dispatch({ type: 'GET_VIDEOS', payload }),
  searchVideos: payload => dispatch({ type: 'GET_VIDEOS', payload })
});


class Filters extends React.Component {

  render() {
    const { selectedItem, selectedCategory, year } = this.props;
    return (
      <div className="filters-component" style={{ marginBottom: 20 }}>
        <Paper elevation={0} style={{ padding: 10, borderLeft: '1px solid rgba(34,36,38,.15)', borderRight: '1px solid rgba(34,36,38,.15)', borderBottom: '1px solid rgba(34,36,38,.15)' }}>
          <form autoComplete="off">
            <FormControl fullWidth>
              <InputLabel htmlFor="age-simple">Category</InputLabel>
              <Select
                value={this.props.selectedCategory}
                onChange={event => {
                  this.props.selectCategory(event.target.value)
                  agent.search(selectedItem, this.props.searchVideos, { category: selectedCategory, year: year })
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {this.props.categories.map(item => (
                  <MenuItem key={item.id} value={item.id}>{item.snippet.title}</MenuItem>
                ))}

              </Select>
            </FormControl>
            <FormControl fullWidth>
              <span style={{ marginTop: 10 }}>Year: {this.props.year}</span>
              <Slider
                min={1970}
                max={(new Date()).getFullYear()}
                value={this.props.year}
                onChange={value => {
                  this.props.selectYear(value);
                  agent.search(selectedItem, this.props.searchVideos, { category: selectedCategory, year: year })
                }}
              />
            </FormControl>
          </form>
        </Paper>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
