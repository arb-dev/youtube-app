import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import IconButton from '@material-ui/core/IconButton';
import NumericLabel from 'react-pretty-numbers';

import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    searchInput: state.searchInput,
    videos: [...state.videos]
  }
};

const mapDispatchToProps = dispatch => ({

});

function RenderVideo({ tileData, ...rest }) {
  return (
    <Grid item xs={12} sm={4} md={3}>
      <Card className='card'>
        <IconButton aria-label="Add to favorites" className='favBtn'>
          <FavoriteIcon />
        </IconButton>
        <CardMedia
          component='div'
          className='media'
          image={tileData.snippet.thumbnails.high.url}
          title={tileData.snippet.title}
        />
        <CardContent>
          <Typography gutterBottom component="h2" className='title' title={tileData.title}>
            {tileData.snippet.title}
          </Typography>
          <Typography component="p">
            Author: {tileData.snippet.channelTitle}
          </Typography>
          <Typography component="div">
            Viewers:&nbsp;
            <NumericLabel params={{
              'shortFormat': 'true',
              'shortFormatMinValue': 10000,
              'shortFormatPrecision': 1,
              'cssClass': ['numerical-inline']
            }}>
              {tileData.statistics.viewCount}
            </NumericLabel>
          </Typography>
          <Typography component="div">
            Upvotes :&nbsp;
            <NumericLabel params={{
              'shortFormat': 'true', 'shortFormatMinValue': 10000,
              'shortFormatPrecision': 1,
              'cssClass': ['numerical-inline']
            }}>
              {tileData.statistics.likeCount}
            </NumericLabel>
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  )
}

function renderNoVideos() {
  return (
    <Grid container alignItems='center' justify='center' direction='row'>
      <Grid item xs={6}>
        <Card style={{ margin: 'auto', marginTop: 20, textAlign: 'center' }}>
          <CardContent>
            <Typography component="h2" className='title' title='No videos found'>
              No videos found
        </Typography>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

class GridList extends React.Component {
  render() {
    return (
      <div className='grid'>
        <Grid container alignItems='center' justify='center' direction='row'>
          <Grid item xs={11}>
            <Grid container spacing={24}>
              {
                this.props.videos.length ?
                  this.props.videos.map(tile => (<RenderVideo tileData={tile} key={tile.id} />))
                  : renderNoVideos()
              }
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GridList);
