import React from 'react';
import Search from '../Search/Search';
import SearchGrid from '../Grid/Grid';

class Home extends React.Component {
    render() {
        return (
            <div>
                <Search />
                <SearchGrid />
            </div>
        );
    }
}

export default Home;