import React from 'react';
import { connect } from 'react-redux';
import { Div } from 'glamorous';
import { css } from 'glamor';
import matchSorter from 'match-sorter';
import Downshift from 'downshift';
import {
  Menu,
  ControllerButton,
  Input,
  Item,
  ArrowIcon,
  XIcon
} from './autoComplete';
import agent from '../../agent';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Filters from '../Filters/Filters';

const mapStateToProps = state => {
  return {
    items: state.items,
    selectedItem: state.selectedItem,
    selectedCategory: state.selectedCategory,
    year: state.year
  };
}

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch({ type: 'ADD_ITEM', item }),
  addItems: items => dispatch({ type: 'ADD_ITEMS', items }),
  searchVideos: payload => dispatch({ type: 'GET_VIDEOS', payload }),
  removeItem: item => dispatch({ type: 'REMOVE_ITEM', item })
});

function RenderDownshift({ itemToString, getItems, getSuggestions, searchVideos, agentSearch, ...rest }) {
  return (
    <Downshift
      itemToString={itemToString}
      {...rest}>
      {({
        getInputProps,
        getButtonProps,
        getItemProps,
        isOpen,
        toggleMenu,
        clearSelection,
        selectedItem,
        inputValue,
        highlightedIndex
      }) =>
        <div className={css({ width: 350, margin: 'auto', position: 'relative', marginBottom: 20 })}>
          <Div position="relative" css={{ paddingRight: '1.75em' }}>
            <Input
              {...getInputProps({
                isOpen,
                placeholder: 'Search...',
                inputValue
              })}
            />
            {selectedItem
              ? <ControllerButton
                css={{ paddingTop: 4 }}
                onClick={clearSelection}
                aria-label="clear selection"
              >
                <XIcon />
              </ControllerButton>
              : <div>
                <ControllerButton style={{ right: 8, top: 10, color: 'rgba(0,0,0,0.54)' }} onClick={() => agentSearch(inputValue, searchVideos)}>
                  <SearchIcon />
                </ControllerButton>
              </div>
            }
          </Div>
          {!isOpen
            ? null
            : <Menu css={{ position: 'absolute', background: 'white', zIndex: 10, right: 0, left: 0 }}>
              {getItems(inputValue).map((item, index) =>
                <Item
                  key={item.id}
                  {...getItemProps({
                    item,
                    index,
                    isActive: highlightedIndex === index,
                    isSelected: selectedItem === item
                  })}
                >
                  {itemToString(item)}
                </Item>
              )}
            </Menu>}
          <Filters style={{ marginBottom: 10 }} />
        </div>}
    </Downshift>
  );
}

class SearchContainer extends React.Component {
  getSuggestions = value => {
    agent.getSuggestions(value, result => {
      this.props.addItems(result.slice(0, 9));
    });
  }

  handleChange = (selectedItem, downshiftState) => {
    const { addItem, removeItem } = this.props;

    if (!selectedItem) {
      removeItem(null);
    } else {
      addItem(selectedItem);
      agent.search(selectedItem.name, this.props.searchVideos, { category: this.props.selectedCategory, year: this.props.year });
    }
  };

  handleStateChange = state => {
    if (state.type === '__autocomplete_change_input__') {
      this.getSuggestions(state.inputValue);
    }
  }

  getItems = value => {
    const { items } = this.props;
    return value
      ? matchSorter(items, value, {
        keys: ['name']
      })
      : items;
  };

  itemToString(i) {
    return i ? i.name : '';
  }

  render() {
    const { selectedItem } = this.props;

    return (
      <div className="search-wrapper">
        <RenderDownshift
          selectedItem={selectedItem}
          onStateChange={this.handleStateChange}
          onChange={this.handleChange}
          getItems={this.getItems}
          itemToString={this.itemToString}
          agentSearch={agent.search}
          searchVideos={this.props.searchVideos}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer); 