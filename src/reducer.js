import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import videos from './reducers/grid';
import { getCategories as categories, selectCategory as selectedCategory } from './reducers/categories';
import year from './reducers/year';
import {
  addItem,
  addItems as items,
  removeItem,
  searchVideos
} from './reducers/search';

export default combineReducers({
  videos,
  categories,
  selectedCategory,
  year,
  addItem,
  items,
  removeItem,
  searchVideos,
  router: routerReducer
});
