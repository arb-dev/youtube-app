function getCategories(state = [], action) {
    switch (action.type) {
        case 'GET_CATEGORIES':
            return [...state, ...action.payload]
        default:
            return state;
    }
}

function selectCategory(state = '', action) {
    switch (action.type) {
        case 'GET_CATEGORY':
            return action.category
        default:
            return state;
    }
}

export {
    getCategories,
    selectCategory
}