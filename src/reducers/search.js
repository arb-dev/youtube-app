function addItem(state = {}, action) {
    switch (action.type) {
        case 'ADD_ITEM':
            return Object.assign({}, state, { selectedItem: action.item })

        default:
            return state;
    }
}

function addItems(state = [], action) {
    switch (action.type) {
        case 'ADD_ITEMS':
            return [...action.items]

        default:
            return state;
    }
}

function searchVideos(state = {}, action) {
    switch (action.type) {
        case 'GET_VIDEOS':
            return Object.assign({}, state, { items: action.item })

        default:
            return state;
    }
}

function removeItem(state = {}, action) {
    switch (action.type) {
        case 'REMOVE_ITEM':
            return Object.assign({}, state, { selectedItem: action.item })

        default:
            return state;
    }
}

export {
    addItem,
    addItems,
    removeItem,
    searchVideos
}